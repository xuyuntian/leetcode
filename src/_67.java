public class _67 {
    static class Solution {
        public String addBinary(String a, String b) {
            int i = a.length()-1,j = b.length()-1;
            int flow = 0;
            StringBuilder sb = new StringBuilder();
            while(i >= 0 || j >= 0){
                int x = i >= 0 ? a.charAt(i) -'0' : 0;
                int y = j >= 0 ? b.charAt(j) -'0': 0;
                char cur = (x + y + flow) % 2 == 0 ? '0' : '1';
                flow = (x + y + flow) / 2;
                sb.append(cur);
                i--;
                j--;
            }
            if(flow == 1){
                sb.append('1');
            }
            return sb.reverse().toString();
        }
    }
}
