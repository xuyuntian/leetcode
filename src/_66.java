public class _66 {
    static class Solution{
        public int[] plusOne(int[] digits) {
            //看了好几遍才明白意思
            int flow = 1;
            for(int i = digits.length-1;i >= 0 ;i--){
                if(flow != 1) break;
                flow = (digits[i] + 1) / 10;
                digits[i]  = (digits[i]  + 1) % 10;
            }
            if(flow==1){
                int[] res = new int[digits.length+1];
                System.arraycopy(digits,0,res,1,digits.length);
                res[0] = 1;
                return res;
            }
            return digits;
        }
    }
}
