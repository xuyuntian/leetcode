public class _869 {
    static class Solution {
        public boolean reorderedPowerOf2(int n) {
            //反向思路
            //通过 2 ^ K 来比较 n每一位的数字数量
            //由于1 <= n <= 10 ^ 9;
            //可以用[9,8,7,6.....0]序列的值来表示n的有序数列
            int order = orderNum(n);
            for (int i = 0; i <= 31; i++) {
                if (order == orderNum(1 << i)) return true;
            }
            return false;
        }

        public int orderNum(int n) {
            int res = 0;
            while (n > 0) {
                int dist = n % 10;
                n /= 10;
                res += (int) Math.pow(10, dist);
            }
            return res;
        }
    }
}
