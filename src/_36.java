public class _36 {
    static class Solution {
        public boolean isValidSudoku(char[][] board) {
            boolean[][] area = new boolean[9][9];
            boolean[][] row = new boolean[9][9];
            boolean[][] col = new boolean[9][9];
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    if (board[i][j] == '.') continue;
                    int val = board[i][j] - '1';
                    int aIdx = i / 3 * 3 + j / 3;
                    if (area[aIdx][val] || row[i][val] || col[j][val]) return false;
                    area[aIdx][val] = row[i][val] = col[j][val] = true;
                }
            }
            return true;
        }
    }
}
