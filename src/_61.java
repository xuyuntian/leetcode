public class _61 {
    static class Solution {
        public ListNode rotateRight(ListNode head, int k) {
            //纯逻辑？
            //观察可知，新的heand 在 list的长度 - k + 1处
            ListNode cursor = head;
            ListNode tail = null;
            int size = 0;
            while (cursor != null) {
                size++;
                tail = cursor;
                cursor = cursor.next;
            }
            if (size == 0) return null;
            k = k % size;
            int target = (size - k + 1) % (size + 1);
            int start = 1;
            ListNode newHead = head;
            ListNode pre = null;
            while (start < target) {
                pre = newHead;
                newHead = newHead.next;
                start++;
            }
            if (pre != null) {
                pre.next = null;
                tail.next = head;
            }
            return newHead;

        }
    }

}
