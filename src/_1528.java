public class _1528 {
    static class Solution1 {
        public String restoreString(String s, int[] indices) {
            char[] chs = s.toCharArray();
            for (int i = 0; i < indices.length; i++) {
                chs[indices[i]] = s.charAt(i);
            }
            return String.valueOf(chs);
        }
    }

    static class Solution2 {
        public String restoreString(String s, int[] indices) {
            char[] chs = s.toCharArray();
            for (int i = 0; i < indices.length; i++) {
                while (indices[i] != i) {
                    int target = indices[i];
                    int next = indices[target];
                    swap(chs, i, target);
                    indices[target] = target;
                    indices[i] = next;
                }
            }
            return String.valueOf(chs);
        }

        public void swap(char[] chs, int i, int j) {
            char tmp = chs[i];
            chs[i] = chs[j];
            chs[j] = tmp;
        }
    }
}
