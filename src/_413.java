public class _413 {
    static class Solution {
        public int numberOfArithmeticSlices(int[] nums) {
            //等差数列
            // 连续的数量x大于3，子数组数量 ==  x -2 + x- 3 + .... x- x -1 = (1 + x -2)/2. * (x-2);
            // =  (x-1) * (x-2) /2;
            int count = 0;
            int pre = -1111;
            int res = 0;
            for (int i = 1; i < nums.length; i++) {
                if (nums[i] - nums[i - 1] != pre) {
                    if (count >= 3) {
                        res += (count - 1) * (count - 2) / 2;
                    }
                    count = 2;
                    pre = nums[i] - nums[i - 1];
                } else {
                    count++;
                }
            }
            if (count >= 3) {
                res += (count - 1) * (count - 2) / 2;
            }
            return res;
        }
    }
}
