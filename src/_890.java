import java.util.ArrayList;
import java.util.List;

public class _890 {
    static class Solution1 {
        public List<String> findAndReplacePattern(String[] words, String pattern) {
            List<String> res = new ArrayList<>();
            for (String word : words) {
                //懒得减去 'a'多分配点空间
                char[] wToP = new char[128];
                char[] pToW = new char[128];
                boolean add = true;
                for (int i = 0; i < word.length(); i++) {
                    char w = word.charAt(i);
                    char p = pattern.charAt(i);
                    if (wToP[w] == 0 && pToW[p] == 0) {
                        wToP[w] = p;
                        pToW[p] = w;
                    }
                    if (wToP[w] != p || pToW[p] != w) {
                        add = false;
                        break;
                    }
                }
                if (add) res.add(word);
            }
            return res;
        }

    }
}
