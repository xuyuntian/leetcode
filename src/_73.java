import java.util.Arrays;

public class _73 {
    public void setZeroes(int[][] matrix) {
        //官方给出的特殊算法
        //第 0 行和第0列不使用
        boolean fisrtColZero = false,firstRowZero = false;
        int m = matrix.length;
        int n = matrix[0].length;
        if(matrix[0][0] == 0){
            fisrtColZero = firstRowZero = true;
        }else{
            for(int i = 0;i < m;i++){
                if(matrix[i][0] == 0){
                    fisrtColZero = true;
                    break;
                }
            }
            for(int j = 0;j < n;j++){
                if(matrix[0][j] == 0) {
                    firstRowZero = true;
                    break;
                }
            }
        }
        for(int i = 1;i < m;i++){
            for(int j = 1;j < n;j++){
                if(matrix[i][j] == 0){
                    matrix[i][0] = 0;
                    matrix[0][j] = 0;
                }
            }
        }
        for(int i = 1;i < m;i++){
            for(int j = 1;j < n;j++){
                if(matrix[i][0] == 0 || matrix[0][j] == 0){
                    matrix[i][j] = 0;
                }
            }
        }
        if(firstRowZero) Arrays.fill(matrix[0],0);
        for(int j = 0;j < m && fisrtColZero;j++){
            matrix[j][0] = 0;
        }
    }
}
