public class _108 {
    static class Solution1 {
        public TreeNode sortedArrayToBST(int[] nums) {
            //递归，每次取中间
            return build(nums, 0, nums.length - 1);
        }

        public TreeNode build(int[] nums, int l, int r) {
            if (l > r) return null;
            int mid = l + (r - l) / 2;
            TreeNode root = new TreeNode(nums[mid]);
            root.left = build(nums, l, mid - 1);
            root.right = build(nums, mid + 1, r);
            return root;
        }
    }
}
