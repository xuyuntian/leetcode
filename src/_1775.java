public class _1775 {
    static class Solution1 {
        /**
         *  要让 num1 中的和和nums2中的和一样,那么先比较大小
         *  对于值大的，降低它的值
         *  对于值小的，增加它的值
         *  由于nums1和nums2的数据范围在1 ～6
         *  对于增加1的值到6和降低6的值到1是等价的，2，5  3，4，  4,3   5，2都是等价的
         *  先统计1 ~6的数量，然后根据和的大小来判断该降低，或上升哪个值，
         *  采取贪心策略，从5开始逐渐降低到1，观察能否快速使和一样。
         */
        public int minOperations(int[] nums1, int[] nums2) {
            int sum1 = 0;
            int sum2 = 0;
            int[] count1 = new int[6];
            int[] count2 = new int[6];
            for (int val : nums1) {
                sum1 += val;
                count1[val - 1]++;
            }
            for (int val : nums2) {
                sum2 += val;
                count2[val - 1]++;
            }
            if (sum1 == sum2) return 0;
            int res = 0;
            int target = Math.abs(sum1 - sum2);
            //贪心每次走最大步数
            for (int i = 5; i >= 1; i--) {
                int count = sum1 > sum2 ? count1[i] + count2[5 - i] : count1[5 - i] + count2[i];
                if (count * i >= target) {
                    return res + (target + i - 1) / i;//取消二目运算符 如果 target % i == 1,则 多出的1会直接让结果增加1
                } else {
                    res += count;
                    target -= count * i;
                }
            }
            return -1;
        }
    }
}
