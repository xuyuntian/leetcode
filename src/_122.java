public class _122 {
    static class Solution1 {
        public int maxProfit(int[] prices) {
            //贪心，大于前面价格可以直接卖，对于连续增长，它们差值就是利润。
            int profit = 0;
            for (int i = 1; i < prices.length; i++) {
                if (prices[i] > prices[i - 1]) {
                    profit += prices[i] - prices[i - 1];
                }
            }
            return profit;

        }
    }

    static class Solution2 {
        public int maxProfit(int[] prices) {
            //dp[status][i]代表当前最大利润，0代表不持有，1代表持有
            //dp[0][i] = Math.max(dp[1][i-1]+prices[i],dp[0][i-1]);
            //dp[1][i] = Math.max(dp[1][i-1],dp[0][i-1]-prices[i]);
            //这里做出简化，不需要用保留所有状态，只需要保留前一个的状态即可
            int hold = Integer.MIN_VALUE;
            int notHold = 0;
            for (int i = 0; i < prices.length; i++) {
                int nextHold = Math.max(hold, notHold - prices[i]);
                notHold = Math.max(hold + prices[i], notHold);
                hold = nextHold;
            }
            return notHold;

        }
    }
}
