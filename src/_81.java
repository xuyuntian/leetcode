public class _81 {
    static class Solution {
        public boolean search(int[] nums, int target) {
            //找最小值的位置？
            int l = 0, r = nums.length - 1;
            int limit = nums[0];
            if (target == limit) return true;
            // 当左右2边边界值一样，那么，只需要剔除它就容易判断了
            while (l < nums.length && nums[l] == limit) {
                l++;
            }
            while (r >= 0 && nums[r] == limit) {
                r--;
            }
            int s = l;
            int e = r;
            int mid = -1;
            //案例分析:
            //target在: 1. 左侧 2.右侧
            //1.
            //  mid在左侧：
            //      nums[mid] >target:
            //          r = mid -1;
            //      else: l = mid + 1;
            //  在右侧：
            //     l = mid + 1;
            //2. mid在右侧：
            //。   略
            //    在左侧: r = mid -1;
            while (l <= r) {
                mid = l + (r - l) / 2;
                if (nums[mid] == target) return true;
                //合并了在同侧的结果
                if ((target > limit && nums[mid] > limit) || (target < limit && nums[mid] < limit)) {
                    if (nums[mid] > target) r = mid - 1;
                    else l = mid + 1;
                } else {
                    //目标在对侧
                    if (nums[mid] > limit) l = mid + 1;
                    else r = mid - 1;
                }
            }
            return false;
        }

    }
}
