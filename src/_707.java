public class _707 {
    static class MyLinkedList {
        static class Node {
            int val;
            Node next;

            Node(int val) {
                this.val = val;
            }
        }

        private Node head;
        private Node tail;
        int size = 0;

        /**
         * Initialize your data structure here.
         */
        public MyLinkedList() {
        }

        /**
         * Get the value of the index-th node in the linked list. If the index is invalid, return -1.
         */
        public int get(int index) {
            if (index >= size) return -1;
            Node res = head;
            for (int i = 0; i < index; i++) {
                res = res.next;
            }
            // System.out.println(this);
            return res.val;

        }

        /**
         * Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
         */
        public void addAtHead(int val) {
            Node cur = new Node(val);
            if (head == null) {
                tail = head = cur;
            } else {
                cur.next = head;
                head = cur;
            }
            size++;
            //   System.out.println(this);
        }

        public String toString() {
            Node cur = head;
            StringBuilder sb = new StringBuilder();
            while (cur != null) {
                sb.append(cur.val + "->");
                cur = cur.next;
            }
            return sb.toString();
        }

        /**
         * Append a node of value val to the last element of the linked list.
         */
        public void addAtTail(int val) {
            Node cur = new Node(val);
            if (tail == null) {
                head = tail = cur;
            } else {
                tail.next = cur;
                tail = cur;
            }
            size++;
            //  System.out.println(this);
        }

        /**
         * Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
         */
        public void addAtIndex(int index, int val) {
            if (index > size) return;
            Node cur = new Node(val);
            if (head == null) {
                head = tail = cur;
                size++;
            } else {
                if (index == size) {
                    addAtTail(val);
                } else if (index == 0) {
                    addAtHead(val);
                } else {
                    Node pre = head;
                    for (int i = 1; i < index; i++) {
                        pre = pre.next;
                    }
                    cur.next = pre.next;
                    pre.next = cur;
                    size++;
                }
            }
            //  System.out.println(this);
        }

        /**
         * Delete the index-th node in the linked list, if the index is valid.
         */
        public void deleteAtIndex(int index) {
            if (index >= size) return;
            if (index == 0) {
                if (head == tail) tail = null;
                head = head.next;
            } else {
                Node pre = head;
                for (int i = 0; i < index - 1; i++) {
                    pre = pre.next;
                }
                if (size == index + 1) {
                    tail = pre;
                }
                pre.next = pre.next.next;
            }
            size--;
            //  System.out.println(this);
        }
    }
}
