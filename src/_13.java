public class _13 {
    static class Solution{
        public int romanToInt(String s) {
            int[] map = new int[128];
            char[] symbols = {'I','V','X','L','C','D','M'};
            int[] values = {1,5,10,50,100,500,1000};
            for(int i = 0;i < symbols.length;i++){
                map[symbols[i]] = values[i];
            }
            char[] ss = s.toCharArray();
            int res = 0;
            char[] first = {'I','X','C'};
            char[] second = {'V','X','L','C','D','M'};
            for(int i = 0;i < ss.length;i++){
                boolean flag = false;
                for(int j = 0;j < first.length;j++){
                    if(i+1 < ss.length &&ss[i] == first[j]){
                        if(second[j*2] == ss[i+1] || second[j*2+1] ==ss[i+1] ){
                            res += map[ss[i+1]] - map[ss[i]];
                            i++;
                            flag =true;
                            break;
                        }
                    }
                }
                if(!flag) res += map[ss[i]];
            }
            return res;
        }
    }
}
