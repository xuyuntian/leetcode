public class _1915 {
    static class Solution {
        public long wonderfulSubstrings(String word) {
            //位运算，discuss大佬的思路
            //a -j 有10位，最多0~1023
            long[] count = new long[1024];
            count[0] = 1L;//针对cur == 0 的情况
            int cur = 0;
            long res = 0L;
            for(int i = 0;i < word.length();i++){
                cur ^=  (1 << (word.charAt(i)-'a'));
                res += count[cur]++; //cur ^ 0 == cur,统计0
                for(int j = 0;j < 10;j++){
                    res += count[cur ^ (1 <<j)];//统计每个bit位只出现一个1的情况
                }
            }
            return res;
        }
    }
}
