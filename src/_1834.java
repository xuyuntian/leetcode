import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class _1834 {
    static class Solution{
        public int[] getOrder(int[][] tasks) {
            //优先队列 (enqueueTime,processingTime,idx)
            //  if(tasks.length == 0) return new int[0];
            Comparator<int[]> pqComparator = (a, b)->{
                if(a[1] == b[1]) return a[2] - b[2];
                return a[1] - b[1];
            };
            int[][] items = new int[tasks.length][3];
            for(int i = 0;i < tasks.length;i++){
                items[i][0] = tasks[i][0];
                items[i][1] = tasks[i][1];
                items[i][2] = i;
            }
            Arrays.sort(items,(a, b)->a[0]-b[0]);
            PriorityQueue<int[]> pq = new PriorityQueue<>(pqComparator);
            int idx = 0;
            int idx2 = 0;
            int startTime = -1;
            int[] res = new int[tasks.length];
            while(idx < items.length || !pq.isEmpty()){
                if(pq.isEmpty() && idx < items.length  &&items[idx][0] > startTime){
                    startTime = items[idx][0];
                }
                while(idx < tasks.length && items[idx][0] <= startTime)
                    pq.offer(items[idx++]);
                int[] cur = pq.poll();
                res[idx2++] = cur[2];
                startTime += cur[1];
            }
            return res;
        }
    }
}
