/**
 * 原题 https://leetcode.com/problems/count-good-nodes-in-binary-tree/
 */
public class _1148 {

    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    /**
     * 深度优先搜索，时间复杂度O(N)
     */
    static class Solution1 {
        public int goodNodes(TreeNode root) {
            return dfs(root, Integer.MIN_VALUE);
        }

        public int dfs(TreeNode root, int max) {
            int res = 0;
            if (root == null) return res;
            if (root.val >= max) res++;
            int newMax = Math.max(root.val, max);
            res += dfs(root.left, newMax);
            res += dfs(root.right, newMax);
            return res;
        }
    }
}
