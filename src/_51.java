import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _51 {
    static class Solution1 {
        public List<List<String>> solveNQueens(int n) {
            char[][] chs = new char[n][n];
            for (char[] arr : chs) {
                Arrays.fill(arr, '.');
            }
            List<List<String>> res = new ArrayList<>();
            dfs(chs, res, new boolean[2 * n], new boolean[2 * n], new boolean[2 * n], 0, n);
            return res;
        }

        public void dfs(char[][] chs, List<List<String>> res, boolean[] x, boolean[] y, boolean[] cols, int row, int n) {
            if (row == n) {
                List<String> resItem = new ArrayList<>();
                for (char[] arr : chs) {
                    resItem.add(new String(arr));
                }
                res.add(resItem);
                return;
            }
            for (int col = 0; col < n; col++) {
                int xIdx = row + col;
                int yIdx = row - col + n;
                if (x[xIdx] || y[yIdx] || cols[col]) continue;
                x[xIdx] = y[yIdx] = cols[col] = true;
                chs[row][col] = 'Q';
                dfs(chs, res, x, y, cols, row + 1, n);
                x[xIdx] = y[yIdx] = cols[col] = false;
                chs[row][col] = '.';
            }
        }
    }
}
