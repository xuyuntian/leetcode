public class _1026 {
    static class Solution {
        public int maxAncestorDiff(TreeNode root) {
            return dfs(root, Integer.MAX_VALUE, Integer.MIN_VALUE);
        }

        public int dfs(TreeNode root, int min, int max) {
            if (root == null) return max - min;
            min = Math.min(root.val, min);
            max = Math.max(root.val, max);
            return Math.max(dfs(root.left, min, max), dfs(root.right, min, max));
        }
    }
}
