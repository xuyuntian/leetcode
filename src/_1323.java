public class _1323 {
    static class Solution {
        public int maximum69Number(int num) {
            int pos = 0;
            int maxPos = -1;
            int cur = num;
            while (cur > 0) {
                int dist = cur % 10;
                cur /= 10;
                if (dist == 6) {
                    maxPos = pos;
                }
                pos++;
            }
            return num + (maxPos != -1 ? (int) (3 * Math.pow(10, maxPos)) : 0);
        }
    }
}