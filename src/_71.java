import java.util.ArrayList;
import java.util.List;

public class _71 {
    class Solution {
        public String simplifyPath(String path) {
            // x, y分别代表'.' 和'字符'的数量
            int x = 0,y = 0;
            char[] ps = path.toCharArray();
            List<String> list = new ArrayList<>();
            StringBuilder sb = new StringBuilder();
            for(int i = 1;i < ps.length;i++){
                char cur = ps[i];
                if(cur != '/') sb.append(cur);
                if(cur == '.'){
                    x++;
                }else if(cur =='/'){
                    if(y !=0 || x > 2){
                        list.add(sb.toString());
                    }else if(x == 2 &&list.size() > 0) list.remove(list.size()-1);
                    sb = new StringBuilder();
                    x = y = 0;
                }else{
                    y++;
                }
            }
            if(y != 0 || x > 2) list.add(sb.toString());
            else if( x == 2 && list.size() > 0)list.remove(list.size()-1);
            if(list.size() == 0) return "/";
            sb = new StringBuilder();
            for(String s : list){
                sb.append('/');
                sb.append(s);
            }
            return sb.toString();
        }
    }
}
