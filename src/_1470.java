public class _1470 {
    static class Solution1 {
        public int[] shuffle(int[] nums, int n) {
            int x = 0;
            int y = nums.length / 2;
            int[] res = new int[nums.length];
            for (int i = 0; i < nums.length; i++) {
                res[i] = i % 2 == 0 ? nums[x++] : nums[y++];
            }
            return res;
        }
    }

    static class Solution2 {
        public int[] shuffle(int[] nums, int n) {
            //位运算，
            //将 xn堆到yn的高位
            //1 <= nums[i] <= 10^3,所以用[1,10]位代表y,[11,20]位代表x
            //迭代[n-1,nums.length]
            //nums[i] = nums[i+n] & mask; i为奇数
            //nums[i]  = nums[i+n] >> 10;i为偶数
            int maskY = 1023;
            for (int i = 0; i < n; i++) {
                nums[i + n] = (nums[i] << 10) | nums[i + n];
            }
            for (int i = 0, j = n; i < nums.length; i += 2, j++) {
                nums[i] = nums[j] >> 10;
                nums[i + 1] = nums[j] & maskY;
            }
            return nums;
        }
    }
}
