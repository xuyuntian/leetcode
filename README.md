# leetcode记录

## index
|  题目   | 代码  | 解法|
|  ----  | ----  |----|
| [5. Longest Palindromic Substring](https://leetcode.com/problems/longest-palindromic-substring/)  | [_5.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_5.java) |[动态规划 ,爆破,中心开花]|
|[13. Roman to Integer](https://leetcode.com/problems/roman-to-integer/)|[_13.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_13.java)|[map]|
|[17. Letter Combinations of a Phone Number](https://leetcode.com/problems/letter-combinations-of-a-phone-number/)|[_17.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_17.java)|[dfs,queue]|
|[28. Implement strStr()](https://leetcode.com/problems/implement-strstr/)|[_28.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_28.java)|[kmp]|
|[36. Valid Sudoku](https://leetcode.com/problems/valid-sudoku/)|[_36.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_36.java)|[set]|
|[44. Wildcard Matching](https://leetcode.com/problems/wildcard-matching/)|[_44.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_44.java)|[dp,dfs+memorize]|
|[51. N-Queens](https://leetcode.com/problems/n-queens/)|[_51.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_51.java)|[dfs+memorize]|
|[52. N-Queens II](https://leetcode.com/problems/n-queens-ii/)|[_52.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_52.java)|[暴力dfs ,dp+dfs]|
|[54. Spiral Matrix](https://leetcode.com/problems/spiral-matrix/)|[_54.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_54.java)|[逻辑]|
|[57. Insert Interval](https://leetcode.com/problems/insert-interval/)|[_57.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_57.java)|[双指针]|
|[61. Rotate List](https://leetcode.com/problems/rotate-list/)|[_61.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_61.java)|[逻辑]|
|[66. Plus One](https://leetcode.com/problems/plus-one/)|[_66.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_66.java)|[进位]|
|[67. Add Binary](https://leetcode.com/problems/add-binary/)|[_67.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_67.java)|[进位]|
|[68. Text Justification](https://leetcode.com/problems/text-justification/)|[_68.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_68.java)|[逻辑]|
|[71. Simplify Path](https://leetcode.com/problems/simplify-path/)|[_71.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_71.java)|[状态机]|
|[73. Set Matrix Zeroes](https://leetcode.com/problems/set-matrix-zeroes/)|[_73.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_73.java)|[矩阵]|
|[77. Combinations](https://leetcode.com/problems/combinations/)|[_77.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_77.java)|[dfs,queue]|
|[81. Search in Rotated Sorted Array II](https://leetcode.com/problems/search-in-rotated-sorted-array-ii/)|[_81.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_81.java)|[BinarySearch]|
|[86. Partition List](https://leetcode.com/problems/partition-list/)|[_86.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_86.java)|[链表]|
|[88. Merge Sorted Array](https://leetcode.com/problems/merge-sorted-array/)|[_88.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_88.java)|[双指针]|
|[93. Restore IP Addresses](https://leetcode.com/problems/restore-ip-addresses/)|[_93.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_93.java)|[爆破,dfs]|
|[95. Unique Binary Search Trees II](https://leetcode.com/problems/unique-binary-search-trees-ii/)|[_95.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_95.java)|[递归]|
|[97. Interleaving String](https://leetcode.com/problems/interleaving-string/)|[_97.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_97.java)|[dp,dfs+memorize]|
|[99. Recover Binary Search Tree](https://leetcode.com/problems/recover-binary-search-tree/)|[_99.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_99.java)|[中序遍历]|
|[103. Binary Tree Zigzag Level Order Traversal](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/)|[_103.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_103.java)|[bfs,dfs]|
|[106. Construct Binary Tree from Inorder and Postorder Traversal](https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/)|[_106.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_106.java)|[递归]|
|[107. Binary Tree Level Order Traversal II](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/)|[_107.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_107.java)|[bfs,dfs]|
|[108. Convert Sorted Array to Binary Search Tree](https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/)|[_108.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_108.java)|[递归]|
|[109. Convert Sorted List to Binary Search Tree](https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/)|[_109.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_109.java)|[递归]|
|[115. Distinct Subsequences](https://leetcode.com/problems/distinct-subsequences/)|[_115.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_115.java)|[dfs+memorize ,dp]|
|[116. Populating Next Right Pointers in Each Node](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/)|[_116.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_116.java)|[dfs,bfs,迭代]|
|[117. Populating Next Right Pointers in Each Node II](https://leetcode.com/problems/populating-next-right-pointers-in-each-node-ii/)|[_117.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_117.java)|[bfs,迭代]|
|[122. Best Time to Buy and Sell Stock II](https://leetcode.com/problems/best-time-to-buy-and-sell-stock-ii/)|[_122.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_122.java)|[greedy,dp]|
|[132. Palindrome Partitioning II](https://leetcode.com/problems/palindrome-partitioning-ii/)|[_132.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_132.java)|[dp,2种不同计算回文方式]|
|[134. Gas Station](https://leetcode.com/problems/gas-station/)|[_134.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_134.java)|[爆破,排除法]|
|[135. Candy](https://leetcode.com/problems/candy/)|[_135.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_135.java)|[dp,one-pass,极小值]|
|[164. Maximum Gap](https://leetcode.com/problems/maximum-gap/)|[_164.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_164.java)|[桶排序]|
|[230. Kth Smallest Element in a BST](https://leetcode.com/problems/kth-smallest-element-in-a-bst/)|[_230.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_230.java)|[中序遍历,stack]|
|[315. Count of Smaller Numbers After Self](https://leetcode.com/problems/count-of-smaller-numbers-after-self/)|[_315.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_315.java)|[归并排序,未知逻辑]|
|[336. Palindrome Pairs](https://leetcode.com/problems/palindrome-pairs/)|[_336.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_336.java)|[Tire,HashMap]|
|[373. Find K Pairs with Smallest Sums]( https://leetcode.com/problems/find-k-pairs-with-smallest-sums/)|[_373.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_373.java)|[优化后的暴力搜索,优先队列]|
|[378. Kth Smallest Element in a Sorted Matrix]( https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/)|[_378.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_378.java)|[二分查找,优先队列]|
|[413. Arithmetic Slices](https://leetcode.com/problems/arithmetic-slices/)|[_413.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_413.java)|[计数器]|
|[480. sliding-window-median](https://leetcode.com/problems/sliding-window-median/)|[_480.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_480.java)|[双优先队列,TreeMap+外部计数器,TreeSet存下标]|
|[508. Most Frequent Subtree Sum](https://leetcode.com/problems/most-frequent-subtree-sum/)|[_508.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_508.java)|[后序遍历递归写法,后序遍历stack写法]|
|[654. Maximum Binary Tree]( https://leetcode.com/problems/maximum-binary-tree/)|[_654.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_654.java)|[递归,stack]|
|[707. Design Linked List](https://leetcode.com/problems/design-linked-list/)|[_707.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_707.java)|[链表]|
|[721. Accounts Merge](https://leetcode.com/problems/accounts-merge/)|[_721.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_721.java)|[bfs,Union-Find]|
|[834. Sum of Distances in Tree](https://leetcode.com/problems/sum-of-distances-in-tree/)|[_834.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_834.java)|[dfs]|
|[864. shortest-path-to-get-all-keys]( https://leetcode.com/problems/shortest-path-to-get-all-keys/)|[_864.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_864.java)|[广度优先搜索,深度优先搜索]|
|[869. Reordered Power of 2]( https://leetcode.com/problems/reordered-power-of-2/)|[_869.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_869.java)|[数学]|
|[890. Find and Replace Pattern](https://leetcode.com/problems/find-and-replace-pattern/)|[_890.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_890.java)|[hash]|
|[942. DI String Match](https://leetcode.com/problems/di-string-match/)|[_942.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_942.java)|[计数器+反转有序数组,贪心+双指针]|
|[998. Maximum Binary Tree II]( https://leetcode.com/problems/maximum-binary-tree-ii/)|[_998.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_998.java)|[循环,递归]|
|[1026. Maximum Difference Between Node and Ancestor](https://leetcode.com/problems/maximum-difference-between-node-and-ancestor/)|[_1026.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1026.java)|[先序遍历]|
|[1092. Shortest Common Supersequence](https://leetcode.com/problems/shortest-common-supersequence/)|[_1092.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1092.java)|[动态规划]|
|[1143. Longest Common Subsequence](https://leetcode.com/problems/longest-common-subsequence/)|[_1143.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1143.java)|[动态规划(自底向上，自顶向下)]|
|[1148. count-good-nodes-in-binary-tree](https://leetcode.com/problems/count-good-nodes-in-binary-tree/)|[_1148.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1148.java) |[深度优先搜索]|
|[1248. Count Number of Nice Subarrays](https://leetcode.com/problems/count-number-of-nice-subarrays/)|[_1248.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1248.java)|[Sliding window ,PrefixSum]|
|[1323. Maximum 69 Number](https://leetcode.com/problems/maximum-69-number/)|[_1323.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1323.java)|[单个变量]|
|[1442. Count Triplets That Can Form Two Arrays of Equal XOR](https://leetcode.com/problems/count-triplets-that-can-form-two-arrays-of-equal-xor/)|[_1442.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1442.java)|[bit-x-or + prefixSum]|
|[1470. Shuffle the Array](https://leetcode.com/problems/shuffle-the-array/)|[_1470.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1470.java)|[双指针,位运算]|
|[1528. Shuffle String](https://leetcode.com/problems/shuffle-string/)|[_1528.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1528.java)|[直接复制,连续交换]|
|[1765. Map of Highest Peak](https://leetcode.com/problems/map-of-highest-peak/)|[_1765.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1765.java)|[bfs]|
|[1775. Equal Sum Arrays With Minimum Number of Operations](https://leetcode.com/problems/equal-sum-arrays-with-minimum-number-of-operations/)|[_1775.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1775.java)|[贪心]|
|[1834. Single-Threaded CPU](https://leetcode.com/problems/single-threaded-cpu/)|[_1834.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1834.java)|[优先队列]|
|[1915. Number of Wonderful Substrings](https://leetcode.com/problems/number-of-wonderful-substrings/)|[_1915.java](https://gitee.com/xuyuntian/leetcode/blob/master/src/_1915.java)|[bit-x-or+prefixSum]|


## 需要强化的思路
|  题目   | 思路  |discuss| 状态|
|  ----  | ----  | ----|----|
|[44. Wildcard Matching](https://leetcode.com/problems/wildcard-matching/)|常量空间复杂度|[链接](https://leetcode.com/problems/wildcard-matching/discuss/17810/Linear-runtime-and-constant-space-solution)|待解决|
|[95. Unique Binary Search Trees II](https://leetcode.com/problems/unique-binary-search-trees-ii/)|动态规划解法|[链接](https://leetcode.com/problems/unique-binary-search-trees-ii/discuss/31493/Java-Solution-with-DP)|待解决|
|[99. Recover Binary Search Tree](https://leetcode.com/problems/recover-binary-search-tree/)|Morris Traversal|[链接](https://leetcode.com/problems/recover-binary-search-tree/discuss/32559/Detail-Explain-about-How-Morris-Traversal-Finds-two-Incorrect-Pointer)|待解决|
|[109. Convert Sorted List to Binary Search Tree](https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/)|O(lgN)空间复杂度|[链接](https://leetcode.com/problems/convert-sorted-list-to-binary-search-tree/discuss/35472/Share-my-O(1)-space-and-O(n)-time-Java-code)|待解决|
|[654. Maximum Binary Tree]( https://leetcode.com/problems/maximum-binary-tree/)|stack：时间复杂度O(N)|[链接](https://leetcode.com/problems/maximum-binary-tree/discuss/106156/Java-worst-case-O(N)-solution)|已解决|
|[721. Accounts Merge](https://leetcode.com/problems/accounts-merge/)|Union-Find|[链接](https://leetcode.com/problems/accounts-merge/discuss/109157/JavaC%2B%2B-Union-Find)|已解决|
|[834. Sum of Distances in Tree](https://leetcode.com/problems/sum-of-distances-in-tree/)|dfs|[链接](https://leetcode.com/problems/sum-of-distances-in-tree/discuss/130583/C%2B%2BJavaPython-Pre-order-and-Post-order-DFS-O%28N)|已解决|
